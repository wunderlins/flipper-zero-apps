# Flipper Zero Apps

## Building

```bash
# application name
export app_name=simon_rulez

# some defaults
alias fbt='FBT_NO_SYNC=1 ./fbt COMPACT=1 DEBUG=1 VERBOSE=0 FORCE=1'

# "make clean"
fbt -c

# compile only
fbt fap_$app_name

# upload, run via st-link. Must be in main screen, else launch fails.
fbt launch_app APPSRC=$app_name
```

## Refenrecnes

- [example FAP](https://github.com/csBlueChip/FlipperZero_plugin_howto)
- [ST Link V2 Debugger](https://www.drewgreen.net/debugging-flipper-zero-firmware-with-st-link-v2/)
- [Awesome](https://github.com/djsime1/awesome-flipperzero)
- [Plugin / Application tutorial](https://github.com/DroomOne/Flipper-Plugin-Tutorial)
- [Wiring esp8266/esp32 to GPIO](https://github.com/UberGuidoZ/Flipper/tree/main/GPIO)
- [ubnleashed firmware](https://github.com/DarkFlippers/unleashed-firmware)
- [Discord](https://discord.com/invite/y5E5m8jbgb)
- [Legic Prime](https://github.com/zhovner/proxmark3-1/blob/master/client/scripts/legic.lua)
  - [cmdhflegic.c](https://github.com/zhovner/proxmark3-1/blob/master/client/cmdhflegic.c)
  - [legic.h](https://github.com/zhovner/proxmark3-1/blob/master/include/legic.h)
  - [legic_prng.h](https://github.com/zhovner/proxmark3-1/blob/master/include/legic_prng.h)

## ESP32-S2 WROVER

![ESP32-S2WROVER](https://user-images.githubusercontent.com/57457139/201228294-d5565fff-99ae-44ed-8076-36b0fc8d39c9.jpg)

## ESP32-S2 WROVER Pinout (thanks max_fpv_austria!)

    ESP32: 44 (RX0) -> FZ: 13 or 15 (these are TX pins)
    ESP32: 43 (TX0) -> FZ: 14 or 16 (these are RX pins)
    ESP32: GND      -> FZ: 8 or 11 or 18 (GND)
    ESP32: 3.3V     -> FZ: 9 (3.3V)

Switch between UART Pins 13/14 and 15/16 setting in FlipperZero
`(GPIO -> USB-UART Bridge -> LEFT -> UART Pins)`

Picture includes wiring pinout for the SD card mod too!

## ESP32-S2 WROVER Pinout (thanks max_fpv_austria!)

    ESP32: 44 (RX0) -> FZ: 13 or 15 (these are TX pins)
    ESP32: 43 (TX0) -> FZ: 14 or 16 (these are RX pins)
    ESP32: GND      -> FZ: 8 or 11 or 18 (GND)
    ESP32: 3.3V     -> FZ: 9 (3.3V)

Switch between UART Pins 13/14 and 15/16 setting in FlipperZero
`(GPIO -> USB-UART Bridge -> LEFT -> UART Pins)`

Picture includes wiring pinout for the SD card mod too!

# ESP32-CAM (Camera)

![ESP32-CAM](https://user-images.githubusercontent.com/57457139/182571081-81df66a8-a536-426f-9ee0-ee277da5ef0a.png)

## ESP32-CAM Pinout to Flipper Zero

    CAM32: 1 (RX0)  -> FZ: 13 or 15 (these are TX pins)
    CAM32: 2 (TX0)  -> FZ: 14 or 16 (these are RX pins)
    CAM32: 3 (GND)  -> FZ: 8 or 11 or 18 (GND)
    CAM32: 4 (3.3V) -> FZ: 9 (3.3V) ***OR*** ESP32: 5V -> FZ: 1 (5V)
